const express = require('express')
const app = express()
const port = 8080
var cors = require('cors')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
const customer_route = require('./routes/customer_route')
const contact_details_route = require('./routes/contact_details_route')
const customer_document_route = require('./routes/customer_document_route')
const customer_types_route = require('./routes/customer_type_route')

app.use('/customer',customer_route)
app.use('/contact',contact_details_route)
app.use('/document',customer_document_route)
app.use('/customer/types',customer_types_route)

app.listen(port, () => {
    console.log(`server started ....! ${port}`)
})