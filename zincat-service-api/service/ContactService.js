const connection = require("../db/dbConnection");

const saveContact = (req, res, connection)=>{
   return connection.query('insert into contact values?',
        [req.body.contact_details.map(item =>
            [item.contact_id, item.contact_name, item.designation, item.email,item.mobile_no,item.code],
        )],

        (err) => {
            if (err) {
                return connection.rollback(() => {
                    connection.release();
                    res.send("Inserting to contact table failed")
                    return reject("Inserting to contact table failed");
                });
            }
            return connection.commit((err) => {
                if (err) {
                    return connection.rollback(() => {
                        connection.release();
                        res.send("Commit failed")
                        return reject("Commit failed");
                    });
                }
                res.send('customer placed success !')
                connection.release();
            });
        })
}

const getContactLastId=(req,res)=>{
    var lastId;
    connection.query('select max(contact_id) as lastId from contact',(err, rows, fields) =>{
        if (err) console.log(err)
        lastId=rows[0].lastId

        if (lastId === undefined || lastId === null) {
            lastId = "CN000000"
        }

        const numericPart = parseInt(lastId.slice(2), 10);
        const newNumericPart = numericPart + 1;
        const newPrimaryKey = "CN" + newNumericPart.toString().padStart(6, "0");
        console.log(newPrimaryKey)
        res.send(newPrimaryKey)
    })
}



module.exports = {saveContact,getContactLastId}