
var connection = require('../db/dbConnection')
var contactService = require('./ContactService')

const saveCustomer = (req, res)=>{

    if (Object.keys(req.body).length !== 0){

        return new Promise((resolve, reject) => {

            connection.getConnection( (err, connection)=> {

                if (err) {
                    res.send("Error occurred while getting the connection")
                    return reject("Error occurred while getting the connection");
                }

                return connection.beginTransaction(err => {

                    if (err) {
                        connection.release();
                        res.send('Error occurred while creating the transaction')
                        return reject("Error occurred while creating the transaction");
                    }

                    return connection.query(

                        'insert into customer values(?,?,?,?,?,?,?,?,?,?,?,?)',
                        [req.body.code, req.body.billing_address, req.body.city,req.body.company_name,req.body.country,
                            req.body.customer_name,req.body.customer_type,req.body.email,req.body.gender,req.body.licence,req.body.mobile_no,
                            req.body.ref_no
                        ],

                        (err) => {

                            if (err) {
                                return connection.rollback(() => {
                                    connection.release();
                                    res.send('Inserting to customer table failed')
                                    return reject("Inserting to customer table failed")
                                });
                            }
                            return contactService.saveContact(req,res,connection)
                        });

                });
            });
        });


    }else {
        res.send("object is empty")
    }

}

const deleteCustomer = (req, res) =>{

    connection.query(
        'select * from contact where code=?',
        [req.body.code],
        (err, rows, fields) => {

            if (err) {
                //res.send('this contact not found')
                console.log(err)
            }
             if(rows.length !== 0){

                 if (Object.keys(req.body).length !== 0){

                     return new Promise((resolve, reject) => {

                         connection.getConnection( (err, connection)=> {

                             if (err) {
                                 res.send("Error occurred while getting the connection")
                                 return reject("Error occurred while getting the connection");
                             }

                             return connection.beginTransaction(err => {

                                 if (err) {
                                     connection.release();
                                     res.send('Error occurred while creating the transaction')
                                     return reject("Error occurred while creating the transaction");
                                 }

                                 return connection.query(
                                     'delete from contact where code=?',
                                     [req.body.code],
                                     (err) => {

                                         if (err) {
                                             return connection.rollback(() => {
                                                 connection.release();
                                                 res.send('contact delete failed')
                                                 return reject("contact delete failed")
                                             });
                                         }

                                         return connection.query('delete from customer where code=?',
                                             [req.body.code],
                                             (err) => {
                                                 if (err) {
                                                     return connection.rollback(() => {
                                                         connection.release();
                                                         res.send("Delete customer failed")
                                                         return reject("Delete customer failed");
                                                     });
                                                 }
                                                 return connection.commit((err) => {
                                                     if (err) {
                                                         return connection.rollback(() => {
                                                             connection.release();
                                                             res.send("Commit failed")
                                                             return reject("Commit failed");
                                                         });
                                                     }
                                                     res.send('customer delete success !')
                                                     connection.release();
                                                 });
                                             })
                                     });

                             });
                         });
                     });



                 }else {
                     res.send("object is empty")
                 }

             }else {
                 res.send('this contact not found ')
             }
        });



}

const getCustomerLastId = (req,res) => {
    var lastId;
    connection.query('select max(code) as lastId from customer',(err, rows, fields) =>{
        if (err) console.log(err)
        lastId=rows[0].lastId

        if (lastId === undefined || lastId === null) {
            lastId = "RE00000"
        }

        const numericPart = parseInt(lastId.slice(2), 10);
        const newNumericPart = numericPart + 1;
        const newPrimaryKey = "RE" + newNumericPart.toString().padStart(5, "0");
        console.log(newPrimaryKey)
        res.send(newPrimaryKey)
    })
}

const getAllCustomers=(req,res)=>{
    connection.query('select * from customer',(err, rows, fields) =>{
        if (err) console.log(err)
        res.send(rows)
    })
}


module.exports = {saveCustomer,deleteCustomer,getCustomerLastId,getAllCustomers}