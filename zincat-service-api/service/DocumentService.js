var pool = require('../db/dbConnection')
const multer = require('multer');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'E:\\zincat\\zincat projects\\zincat project - customer form\\zincat-service-api\\upload');
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const upload2 = multer({ storage }).array('images', 5);

const saveDocuments=(req,res)=>{

    upload2(req, res, (error) => {
        if (error) {
            console.error('Error uploading images:', error);
            res.sendStatus(500);
        } else {

            const images = req.files; // Retrieve the array of uploaded files

            // Save the images to the database
            images.forEach((image) => {
                pool.query(
                    'INSERT INTO images (filename, mimetype, data) VALUES (?, ?, ?)',
                    [image.originalname, image.mimetype, image.buffer],
                    (error, results) => {
                        if (error) {
                            console.error('Error saving image:', error);
                        } else {
                            console.log('Image saved to the database');
                        }
                    }
                );
            });

            res.sendStatus(200);
        }
    });
}

const getDocuments=()=>{
    // Set up a route to retrieve and display the images

}

module.exports = {saveDocuments}