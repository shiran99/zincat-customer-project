const mysql = require('mysql2')

var pool;

function dbConnection(){
    if (!pool){
        pool = mysql.createPool({
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password:'1234',
            database: 'zincat'
        });
    }
    return pool;
}

module.exports = dbConnection();