const express = require('express')
var router = express.Router();


var pool = require('../db/dbConnection')
const multer = require('multer');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'E:\\zincat\\zincat projects\\zincat project - customer form\\zincat-service-api\\upload');
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});

const upload2 = multer({ storage }).array('images', 5);

router.get('/get',(req, res)=>{
    console.log('customer documents to get')
    res.send('customer documents to get')
})

router.get('/get/images', (req, res) => {

    const imageId = req.query.code;
    console.log(imageId)
    // Retrieve the image from the database
    pool.query('SELECT * FROM images WHERE code = ?', [imageId], (error, results) => {
        if (error) {
            console.error('Error retrieving image:', error);
            res.sendStatus(500);
        } else if (results.length === 0) {
            res.sendStatus(404);
        } else {
            //const image = results[0];

            // Set the appropriate Content-Type header for the image
           // res.set('Content-Type', image.mimetype);

            // Send the image data as the server response
            res.json(results);
        }
    });
});


router.post('/post/multiple',(req, res)=>{


    upload2(req, res, (error) => {
        if (error) {
            console.error('Error uploading images:', error);
            res.sendStatus(500);
        } else {



            const images = req.files; // Retrieve the array of uploaded files
            const code = req.body.code;
            console.log(images)
            console.log(code)
            let lastId = code;


            // Save the images to the database
            images.forEach((image) => {
                const numericPart = parseInt(lastId.slice(2), 10);
                const newNumericPart = numericPart + 1;
                const newCode = "RE" + newNumericPart.toString().padStart(5, "0");
                console.log(newCode)
                lastId=newCode
                pool.query(
                    'INSERT INTO images (filename, mimetype, data, code, status) VALUES (?, ?, ?,?,?)',
                    [image.originalname, image.mimetype, image.buffer,code,newCode],
                    (error, results) => {
                        if (error) {
                            console.error('Error saving image:', error);
                        } else {
                            console.log('Image saved to the database');
                        }
                    }
                );
            });

            res.sendStatus(200);
        }
    });

})

router.delete('/delete',(req, res)=>{
    console.log('customer documents to delete')
    res.send('customer documents to delete')
})

router.put('/put',(req, res)=>{
    console.log('customer documents to put ')
    res.send('customer documents to pus ')
})

module.exports = router
