const express = require('express')
var router = express.Router();

var connection = require('../db/dbConnection')
const customerTypeService= require('../service/CustomerTypesService')

router.get('/get',(req, res)=>{
   customerTypeService.getAllCustomerTypes(req,res)
})

router.post('/post',(req, res)=>{
    console.log('customer types to post')
    res.send('customer types to post')
})

router.delete('/delete',(req, res)=>{
    console.log('customer types to delete')
    res.send('customer types to delete')
})

router.put('/put',(req, res)=>{
    console.log('customer types to put ')
    res.send('customer types to pus ')
})




module.exports = router