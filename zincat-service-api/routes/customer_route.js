const express = require('express')
var router = express.Router();
var cors = require('cors')

var customerService = require('../service/CustomerServive')

router.get('/get',cors(),(req, res)=>{
    customerService.getAllCustomers(req,res)
})

router.get('/get/customer/code',(req, res)=>{
    console.log('customer to get')
    customerService.getCustomerLastId(req,res)
})

router.post('/post',(req, res)=>{
    customerService.saveCustomer(req, res)
        .catch(err => {
             console.log(err)
         });
})

router.delete('/delete',(req, res)=>{
    customerService.deleteCustomer(req,res)
})

router.put('/put',(req, res)=>{
    console.log('customer to put ')
    res.send('customer to pus ')
})

module.exports = router
