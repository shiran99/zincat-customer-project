import React from 'react';
import {Button} from "antd";
import './css/mainPage.css'
import SideMenuComp from "../component/SideMenuComp";
import ContentSideComp from "../component/ContentSideComp";
function MainPage(props) {
    return (
        <section className='main'>
            <div className='leftSide'>
                <SideMenuComp/>
            </div>
            <div className='rightSide'>
                <ContentSideComp/>
            </div>
        </section>
    );
}

export default MainPage;