import React, { useEffect, useState } from 'react';
import './css/LoadingScreen.css';
import MainPage from "./mainPage";

const LoadingScreen = () => {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const timer = setTimeout(() => {
            setIsLoading(false);
        }, 1000);

        return () => clearTimeout(timer);
    }, []);

    return (
        <div className="loading-screen-container">
            {isLoading ? (
                <div className="loading-spinner">
                    <div className="circle"></div>
                </div>
            ) : (
               <MainPage/>
            )}
        </div>
    );
};

export default LoadingScreen;
