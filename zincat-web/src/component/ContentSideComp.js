import React, { useState } from 'react';
import CustomerPopUpComp from "./CustomerPopUpComp";
import CustomerTableComp from "./CustomerTableComp";
import CustomModel from "./CustomModel";


function SideMenuComp() {

    return (
        <div >
            <CustomerPopUpComp/>
            <div style={{marginTop:'60px',padding:'40px'}}>
                <CustomModel />
            </div>
        </div>
    );
}

export default SideMenuComp;
