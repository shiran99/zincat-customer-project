import React, { useState } from 'react';
import { Button, Menu } from 'antd';
import {
    AppstoreAddOutlined,
    AppstoreOutlined,
    ContainerOutlined,
    DesktopOutlined, HomeOutlined,
    MailOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    PieChartOutlined,
} from '@ant-design/icons';

function getItem(label, key, icon, children, type,onClick) {
    return {
        key,
        icon,
        children,
        label,
        type,
        onClick,
    };
}

const initialItems = [
    getItem('Zincat Technologies', 'sub1', <HomeOutlined />, [
        getItem('customer', '1',null,null,null,()=>{
            alert('ddddddd')
        }),
        getItem('contact', '2'),
        getItem('services', '3'),
        getItem('accounting', '4'),
    ]),
    getItem('Hotel Management', 'sub2', <AppstoreOutlined />, [
        getItem('Option 5', '5'),
        getItem('Option 6', '6')
    ]),
    getItem('Hospital Management', 'sub3', <MailOutlined />, [
        getItem('Option 7', '7'),
        getItem('Option 8', '8')
    ]),
    getItem('Navigation Two', 'sub4', <AppstoreOutlined />, [
        getItem('Option 9', '9'),
        getItem('Option 10', '10')
    ]),
    getItem('Navigation Two', 'sub5', <PieChartOutlined />, [
        getItem('Option 11', '11'),
        getItem('Option 12', '12')
    ]),
    getItem('Navigation Two', 'sub6', <DesktopOutlined />, [
        getItem('Option 13', '13'),
        getItem('Option 14', '14')
    ]),
    getItem('Navigation Two', 'sub7', <ContainerOutlined />, [
        getItem('Option 15', '15'),
        getItem('Option 16', '16')
    ]),
    getItem('Navigation Two', 'sub8', <AppstoreOutlined />, [
        getItem('Option 17', '17'),
        getItem('Option 18', '18')
    ]),
    getItem('Navigation Two', 'sub9', <AppstoreOutlined />, [
        getItem('Option 19', '19'),
        getItem('Option 20', '20')
    ]),
    getItem('Navigation Two', 'sub10', <AppstoreOutlined />, [
        getItem('Option 21', '21'),
        getItem('Option 22', '22')
    ]),
    getItem('Navigation Two', 'sub11', <AppstoreOutlined />, [
        getItem('Option 23', '23'),
        getItem('Option 24', '24')
    ]),
    getItem('Navigation Two', 'sub12', <AppstoreOutlined />, [
        getItem('Option 25', '25'),
        getItem('Option 26', '26')
    ]),
    getItem('Navigation Two', 'sub13', <AppstoreOutlined />, [
        getItem('Option 27', '27'),
        getItem('Option 28', '28')
    ]),
    getItem('Navigation Two', 'sub14', <AppstoreOutlined />, [
        getItem('Option 29', '29'),
        getItem('Option 30', '30')
    ]),
    getItem('Navigation Two', 'sub15', <AppstoreOutlined />, [
        getItem('Option 31', '31'),
        getItem('Option 32', '32')
    ]),
    getItem('Navigation Two', 'sub16', <AppstoreOutlined />, [
        getItem('Option 33', '33'),
        getItem('Option 34', '34')
    ]),
];

function SideMenuComp() {
    const [collapsed, setCollapsed] = useState(false);
    const [items, setItems] = useState(initialItems);

    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
        if (collapsed) {
            setItems(initialItems);
        } else {
            setItems(items.slice(0, 0));
        }
    };

    return (
        <div style={{ width: '96%', marginTop: '20px' , marginLeft:'5px',position:'relative'}}>
            <Button
                type="primary"
                onClick={toggleCollapsed}
                style={{ marginBottom: 16,width:'42%',height:'45px',position:'absolute',top:0,right:0,left:0,margin:"auto" }}
            >
                {collapsed ? <AppstoreOutlined  style={{fontSize:'20px'}}/> : <AppstoreAddOutlined style={{fontSize:'20px'}}/>}
            </Button>
            <br/>
            <br/>
            <br/>
            <Menu
                defaultSelectedKeys={['']}
                defaultOpenKeys={['']}
                mode="inline"
                theme="dark"
                inlineCollapsed={collapsed}
                items={items}
            />
        </div>
    );
}

export default SideMenuComp;
