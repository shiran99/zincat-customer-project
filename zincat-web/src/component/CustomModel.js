import React, { useState } from 'react';
import {Button, Form, Input, Modal, Upload} from 'antd';
import {PlusOutlined} from "@ant-design/icons";


const CustomModel = () => {

    // this is for model button
    const [isModalOpen, setIsModalOpen] = useState(false);
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel1 = () => {
        setIsModalOpen(false);
    };

    // this is for profile image
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [fileList, setFileList] = useState([]);

    const getBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
            reader.readAsDataURL(file);
        });
    };


    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        setPreviewVisible(true);
        setPreviewImage(file.url || file.preview);
    };
    const handleChange = ({ fileList }) => {
        setFileList(fileList);
    };
    const handleCancel = () => {
        setPreviewVisible(false);
    };
    const uploadButton = (
        <div
            style={{
                backgroundImage: 'url("https://imgv3.fotor.com/images/gallery/AI-3D-Female-Profile-Picture.jpg")',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                width: '100%',
                height: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <PlusOutlined style={{ fontSize: 24,color:'black' }} />
            <div style={{ marginTop: 8 }}></div>
        </div>
    );


    return (
        <>
            <Button type="primary" onClick={showModal}>
                Open Modal
            </Button>
            <Modal title="Basic Modal" open={isModalOpen}
                   onOk={handleOk}
                   onCancel={handleCancel1}
                   width={1200}
            >
                <Form layout="vertical" style={{display:'flex',flexDirection:'row',justifyContent:'',alignItems:'space-around' +
                        
                }}>
                    <Form.Item label='customer name' >
                        <Input/>
                    </Form.Item>
                    <Form.Item label='customer name'>
                        <Input/>
                    </Form.Item>
                    <Form.Item label='customer name'>
                        <Input/>
                    </Form.Item>
                    <Form.Item label='profile image'>
                        <Upload
                            action="/your-upload-api"
                            listType="picture-card"
                            fileList={fileList}
                            onPreview={handlePreview}
                            onChange={handleChange}
                        >
                            {fileList.length >= 1 ? null : uploadButton}
                        </Upload>
                        <Modal open={previewVisible} footer={null} onCancel={handleCancel}>
                            <img alt="Preview" style={{ width: '100%' }} src={previewImage} />
                        </Modal>
                    </Form.Item>
                    <Form.Item label='customer name'>
                        <Input/>
                    </Form.Item>
                    <Form.Item label='customer name'>
                        <Input/>
                    </Form.Item>
                    <Form.Item label='customer name'>
                        <Input/>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};
export default CustomModel;