import {Button, Form, Input, Modal, Space, Image, Upload, Table, Select,Radio} from 'antd';
import React, {useEffect, useState} from 'react';
import {AppstoreOutlined, CloudDownloadOutlined, LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import './css/ModalCss.css'
import TextArea from "antd/es/input/TextArea";
import ImgCrop from 'antd-img-crop';
import axios from "axios";



const PopUpModel = () => {
    const [open, setOpen] = useState(false);

    const [form] = Form.useForm();
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [fileList, setFileList] = useState([]);
    const [customerCode,setCustomerCode] = useState('')
    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }
        setPreviewImage(file.url || file.preview);
        setPreviewVisible(true);
    };

    const handleChange = ({ fileList }) => {
        setFileList(fileList);
    };

    const handleCancel = () => {
        setPreviewVisible(false);
    };

    const getBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        });
    };

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div>
                {fileList.length >= 1 ? null : (
                    <>
                        <img src="https://images.unsplash.com/photo-1529665253569-6d01c0eaf7b6?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8cHJvZmlsZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=500&q=60" alt="Sample"
                             style={{ width: '50%' }} />
                        <div style={{ marginTop: 8 }}>Upload</div>
                    </>
                )}
            </div>
        </div>
    );

    const columns=[
        {
            title:'Name',
            dataIndex:'name'
        },
        {
            title:'Designation',
            dataIndex:'designation'
        },
        {
            title:'Mobile',
            dataIndex:'mobile'
        },
        {
            title:'Email',
            dataIndex:'email'
        }
    ]

    const plainOptions = ['Male', 'Female'];
    const [value1, setValue1] = useState('Apple');

    const onChange1 = ({ target: { value } }) => {
        console.log('radio1 checked', value);
        setValue1(value);
    };
    const [customerType,setCustomerType] = useState('')

    useEffect(()=>{
        getCustomerCode().catch(error=>{
            console.log(error)
        })
        getCustomerTypes().catch(error=>{
            console.log(error)
        })
    },[])

    const getCustomerTypes= async ()=>{
        try {
            const response = await axios.get('http://localhost:8080/customer/types/get');
            console.log(response.data)
            setCustomerType(response.data)
            let cusType=[]
            for (let responseElement of response.data) {
                console.log(responseElement.type_name)
                cusType.push(
                    {
                        value: responseElement.type_name,
                        label: responseElement.type_name,
                    }
                )
                setCustomerType(cusType)
            }
            //setCustomerCode(response.data)
        } catch (error) {
            console.log('Error fetching data:', error);
        }
    }

    const getCustomerCode=  async ()=>{
        try {
            const response = await axios.get('http://localhost:8080/customer/get/customer/code');
            //console.log(response.data)
            setCustomerCode(response.data)
        } catch (error) {
            console.log('Error fetching data:', error);
        }
    }

    const initialValues = {
        code: customerCode, // Replace 'fieldName' with the actual name of the input field
    };

    return (
        <>
            <Button type="primary" onClick={() => setOpen(true)} style={{
                width:'50px',
                height:'40px',
                color:'white',
                background:'rgb(15 129 24)',
                position:'absolute',
                right:'20px',
                top:'10px',
                fontWeight:'bold'
            }}>
                 {<CloudDownloadOutlined style={{fontSize:'20px'}}/>}
            </Button>

            <Button type="primary" onClick={() => setOpen(true)} style={{
                width:'150px',
                height:'40px',
                color:'white',
                background:'rgb(35 26 111)',
                position:'absolute',
                right:'100px',
                top:'10px',
                fontWeight:'bold'
            }}>
                New Customer {<LoadingOutlined style={{fontSize:'20px'}}/>}
            </Button>

            <Modal
                title="CUSTOMER DETAILS:"
                centered
                open={open}
                closable={false}
                onOk={() => setOpen(false)}
                onCancel={() => setOpen(false)}
                width={1000}
            >
                <hr />
                <div style={{ display: 'flex', flexDirection: 'row-reverse'}}>
                    <div style={{ flex: 1, marginLeft: 16, textAlign: 'right' }}>
                        <Upload
                            listType='picture-card'
                            fileList={fileList}
                            onPreview={handlePreview}
                            onChange={handleChange}
                        >
                            {fileList.length >= 1 ? null : uploadButton}
                        </Upload>
                        <Modal open={previewVisible} footer={null} onCancel={handleCancel}>
                            <img alt='Preview' style={{ width: '100%'}} src={previewImage} />
                        </Modal>
                    </div>
                    <Form layout='vertical' style={{ flex: 8 }} initialValues={initialValues}>
                        <Form.Item label='code' name='code' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='code' required />
                        </Form.Item>
                        <Form.Item label='Reference No' name='referenceNo' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='reference no' required />
                        </Form.Item>
                        <Form.Item label='Customer Type' name='customer_type' className='customerForm' style={{ marginBottom: 0 ,width:'23%'}}>
                            <Select
                                showSearch
                               // placeholder="Select a person"
                                optionFilterProp="children"
                                //onChange={onChange}
                                //onSearch={onSearch}
                                filterOption={(input, option) =>
                                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                                }
                                options={customerType}
                            />
                        </Form.Item>
                        <Form.Item label='Customer Name' name='customerName' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='customer name' required />
                        </Form.Item>
                        <Form.Item label='Company Name' name='companyName' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='company name' required />
                        </Form.Item>
                        <Form.Item label='NIC/Passport/Driver Licence' name='licence' className='customerForm' style={{ marginBottom: 0 ,width:'23%'}}>
                            <Input placeholder='NIC/Passport/Driver Licence' required />
                        </Form.Item>
                        <Form.Item label='Billing Address' name='billingAddress' className='customerForm' style={{ marginBottom: 0 ,width:'47%',marginTop:''}}>
                            <TextArea
                                //value={value}
                                //onChange={(e) => setValue(e.target.value)}
                                placeholder="Billing Address"
                                autoSize={{
                                    minRows: 3,
                                    maxRows: 5,
                                }}
                            />
                        </Form.Item>
                        <Form.Item label='Mobile Number' name='mobileNo' className='customerForm' style={{ marginBottom: 0,width:'23%',marginTop:'' }}>
                            <Input placeholder='Mobile Number' required />
                        </Form.Item>
                        <Form.Item label='Email' name='email' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='Email' required />
                        </Form.Item>
                        <Form.Item label='Country' name='country' className='customerForm' style={{ marginBottom: 0, width:'31%' }}>
                            <Select
                                showSearch
                                // placeholder="Select a person"
                                optionFilterProp="children"
                                //onChange={onChange}
                                //onSearch={onSearch}
                                filterOption={(input, option) =>
                                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                                }
                                options={[
                                    {
                                        value: 'sriLanka',
                                        label: 'Sri Lanka',
                                    },
                                    {
                                        value: 'us',
                                        label: 'Us',
                                    },
                                    {
                                        value: 'india',
                                        label: 'India',
                                    },
                                ]}
                            />
                        </Form.Item>
                        <Form.Item label='City' name='City' className='customerForm' style={{ marginBottom: 0 }}>
                            <Input placeholder='city' required />
                        </Form.Item>

                        <Form.Item label='Gender' name='gender' className='customerForm' style={{ marginBottom: 0 }}>
                            <Radio.Group options={plainOptions} onChange={onChange1} value={value1} />
                        </Form.Item>

                    </Form>
                </div>
                <br/>
                <hr/>
                <Form layout='vertical'>
                    <Form.Item label='User Name' name='userName' className='customerForm' style={{ marginBottom: 0 }}>
                        <Input placeholder='user name' required />
                    </Form.Item>
                    <Form.Item label='Designation' name='designation' className='customerForm' style={{ marginBottom: 0 }}>
                        <Input placeholder='designation' required />
                    </Form.Item>
                    <Form.Item label='Mobile' name='mobile' className='customerForm' style={{ marginBottom: 0 }}>
                        <Input placeholder='Mobile' required />
                    </Form.Item>
                    <Form.Item label='Email' name='email' className='customerForm' style={{ marginBottom: 0 }}>
                        <Input placeholder='email' required />
                    </Form.Item>
                    <Form.Item className='customerForm' style={{paddingTop:'26px',paddingLeft:'89px'}}>
                        <Button type='primary'>add</Button>
                    </Form.Item>
                </Form>
                <Table
                    columns={columns}
                    pagination={false}
                />
            </Modal>


        </>
    );
};
export default PopUpModel;