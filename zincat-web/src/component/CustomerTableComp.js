import React, { useState, useEffect } from 'react';
import { Table, Input, Popconfirm, Form } from 'antd';
import axios from 'axios';

const EditableCell = ({
                          editing,
                          dataIndex,
                          title,
                          inputType,
                          record,
                          index,
                          children,
                          ...restProps
                      }) => {
    const inputNode = <Input />;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{ margin: 0 }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                children
            )}
        </td>
    );
};

const SampleTable = () => {
    const [form] = Form.useForm();
    const [data, setData] = useState([]);
    const [editingKey, setEditingKey] = useState('');
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    useEffect(() => {
        fetchData().then(r => {
            console.log(r)});
    }, []);

    const fetchData = async () => {
        try {
            const response = await axios.get('http://localhost:8080/customer/get');
            console.log(response.data)
           setData(response.data);
        } catch (error) {
            console.log('Error fetching data:', error);
        }
    };

    const isEditing = (record) => record.id === editingKey;

    const edit = (record) => {
        form.setFieldsValue({ ...record });
        setEditingKey(record.id);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const save = async (id) => {
        try {
            const row = await form.validateFields();
            const newData = [...data];
            const index = newData.findIndex((item) => id === item.id);

            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, { ...item, ...row });
                setData(newData);
                setEditingKey('');
            } else {
                newData.push(row);
                setData(newData);
                setEditingKey('');
            }
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    const deleteRow = (id) => {
        const newData = [...data];
        const index = newData.findIndex((item) => id === item.id);
        if (index > -1) {
            newData.splice(index, 1);
            setData(newData);
        }
    };

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const getColumnSearchProps = (dataIndex, title) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`Search ${title}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <button
                        type="button"
                        onClick={() => handleReset(clearFilters)}
                        style={{ width: '40%', marginRight: 8 }}
                    >
                        Reset
                    </button>
                    <button
                        type="button"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        style={{ width: '40%' }}
                    >
                        Search
                    </button>
                </div>
            </div>
        ),
        filterIcon: (filtered) => (
            <i style={{ color: filtered ? '#1890ff' : undefined }} className="fa fa-search"></i>
        ),
        onFilter: (value, record) =>
            record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
        render: (text) => searchedColumn === dataIndex ? (
            <span>
    {text}
  </span>
        ) : (
            text ?? ''
        ),
    });

    const columns = [
        {
            title: 'Code',
            dataIndex: 'code',
            editable: true,
            ...getColumnSearchProps('code', 'code'),
        },
        {
            title: 'Type',
            dataIndex: 'customer_type',
            editable: true,
            ...getColumnSearchProps('customer_type', 'customer_type'),
        },
        {
            title: 'Name',
            dataIndex: 'customer_name',
            editable: true,
            ...getColumnSearchProps('customer_name', 'customer_name'),
        },
        {
            title: 'Mobile',
            dataIndex: 'mobile_no',
            editable: true,
            ...getColumnSearchProps('mobile_no', 'mobile_no'),
        },
        {
            title: 'Country',
            dataIndex: 'country',
            editable: true,
            ...getColumnSearchProps('country', 'country'),
        },
        {
            title: 'City',
            dataIndex: 'city',
            editable: true,
            ...getColumnSearchProps('city', 'city'),
        },
        {
            title: 'Gender',
            dataIndex: 'gender',
            editable: true,
            ...getColumnSearchProps('gender', 'gender'),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
            <a href="#" onClick={() => save(record.id)} style={{ marginRight: 8 }}>
              Save
            </a>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
                ) : (
                    <>
                        <a href="#" onClick={() => edit(record)} style={{ marginRight: 8 }}>
                            Edit
                        </a>
                        <Popconfirm
                            title="Are you sure to delete this row?"
                            onConfirm={() => deleteRow(record.id)}
                        >
                            <a>Delete</a>
                        </Popconfirm>
                    </>
                );
            },
        },
    ];

    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: 'text',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });

    const filteredData = data.filter((item) => {
        if (!searchText) return true;
        return columns.some((column) => {
            const dataIndex = Array.isArray(column.dataIndex) ? column.dataIndex[0] : column.dataIndex;
            return item[dataIndex]
                .toString()
                .toLowerCase()
                .includes(searchText.toLowerCase());
        });
    });

    return (
        <Form form={form} component={false}>
            <Input.Search
                placeholder="Search"
                onChange={(e) => setSearchText(e.target.value)}
                style={{ marginBottom: 16 }}
            />
            <Table
                components={{
                    body: {
                        cell: EditableCell,
                    },
                }}
                bordered
                dataSource={filteredData}
                columns={mergedColumns}
                rowKey={'code'}
                rowClassName="editable-row"
                pagination={false}
            />
        </Form>
    );
};

export default SampleTable;
