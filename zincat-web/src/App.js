import LoadingScreen from "./page/LoadingScreen";
import MainPage from "./page/mainPage";

function App() {
  return (
    <div className="App">
      <MainPage/>
    </div>
  );
}

export default App;
